package app.thelema.ecs

interface SimulationListener {
    fun startSimulation() {}

    fun stopSimulation() {}
}