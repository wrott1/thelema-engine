const val thelema_group = "app.thelema"
const val thelema_version = "0.7.0"

const val kotlin_version = "1.5.31"
const val ktor_version = "1.5.2"
const val atomicfu_version = "0.15.0"

const val lwjgl_version = "3.2.3"
const val lwjgl_prefix = "org.lwjgl:lwjgl"